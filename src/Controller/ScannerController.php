<?php

namespace App\Controller;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ScannerController {

  protected $request;
  protected $quarantine_dir;
  protected $infected_dir;
  protected $clean_dir;
  protected $incoming_file;
  protected $file_name;
  protected $fileSystem;

  public function __construct() {
    $this->request = Request::createFromGlobals();
    $this->quarantine_dir = './quarantined';
    $this->infected_dir = './infected';
    $this->clean_dir = './clean';
    $this->incoming_file = self::getIncomingFileFromRequest();
    $this->file_name = self::getIncomingFileName();
    $this->fileSystem = new Filesystem();
  }

  public function processIncomingFile() {
    self::moveIncomingFileToQuarantine();
    $scan = self::runClamscanOnIncomingFile();
    return self::checkScanStatus($scan);
  }

  public function getIncomingFileFromRequest() {
    $file = $this->request->files->get('file_to_scan');
    if (empty($file)) {
      die("No file added to scan");
    }
    return $file;
  }

  public function getIncomingFileName() {
    $filename = $this->incoming_file->getClientOriginalName();
    return $filename;
  }

  public function moveIncomingFileToQuarantine() {
    $this->incoming_file->move($this->quarantine_dir, $this->file_name);
  }

  public function moveCleanFile() {
    $clean_file = $this->quarantine_dir . "/" . $this->file_name;
    $new_clean_file = $this->clean_dir . "/" . $this->file_name;
    $this->fileSystem->rename($clean_file, $new_clean_file);
  }

  public function runClamscanOnIncomingFile() {
    $scan_file = shell_exec("clamscan $this->quarantine_dir/" . $this->file_name . " --move=./infected");
    return $scan_file;
  }

  public function checkScanStatus($scan) {
    $scan_status_split = explode(PHP_EOL, $scan);
    $infected_files_line = $scan_status_split[7];
    switch ($infected_files_line) {
      case (strpos($infected_files_line, "0") !== false):
        self::moveCleanFile();
        return new Response("CLEAN");
      case (strpos($infected_files_line, "1") !== false):
        return new Response("INFECTED");
      default:
        return new Response("Could not detect status for $this->file_name");
    }
  }

  public function index() {
    return new Response("");
  }

}